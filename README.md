# Cool Shops
#### Installation guide
Install necessary packages and build VueJS stuff
```sh
$ composer install
$ npm install
$ npm run dev
```
Then rename .env.example to .env and change values accordingly. Currently there is no Admin Panel as it was not requested so you will have to add data to the db manually.
Run this after editing your .env file.
```sh
$ php artisan migrate
```
<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePreferredShops extends Migration {
	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up() {
		Schema::create('shop_feedback', function (Blueprint $table) {
			$table->bigIncrements('id');
			$table->timestamps();
			$table->integer('user_id');
			$table->integer('shop_id');
			$table->enum('type', ['NONE', 'LIKED', 'DISLIKED'])->default("NONE");
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down() {
		Schema::dropIfExists('shop_feedback');
	}
}

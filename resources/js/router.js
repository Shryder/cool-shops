import VueRouter from 'vue-router';
// Pages
import Home from './pages/Home';
import Register from './pages/Register';
import Login from './pages/Login';
import PreferredShops from './pages/user/PreferredShops';

// Routes
const routes = [{
		path: '/',
		name: 'home',
		component: Home,
		meta: {
			auth: undefined
		}
	},
	{
		path: '/register',
		name: 'register',
		component: Register,
		meta: {
			auth: false
		}
	},
	{
		path: '/login',
		name: 'login',
		component: Login,
		meta: {
			auth: false
		}
	},
	{
		path: '/preferred_shops',
		name: 'preferred_shops',
		component: PreferredShops,
		meta: {
			auth: true
		}
	}
]
const router = new VueRouter({
	history: true,
	mode: 'history',
	routes,
});

export default router

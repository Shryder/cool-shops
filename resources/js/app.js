import 'es6-promise/auto';
import './bootstrap';

import Vue from 'vue';
window.Vue = Vue;

import router from './router';
Vue.router = router;

import VueRouter from 'vue-router';
Vue.use(VueRouter);

import axios from 'axios';
import VueAxios from 'vue-axios';
Vue.use(VueAxios, axios);

axios.defaults.baseURL = `${process.env.MIX_APP_URL}/api`;

import VueAuth from '@websanova/vue-auth';
import auth from './auth';
Vue.use(VueAuth, auth);

import Index from './Index';
Vue.component('index', Index);

const app = new Vue({
	el: '#app',
	router
});

<?php

namespace App\Http\Controllers;

use App\Shop;
use App\ShopFeedback;
use App\User;
use Auth;
use DB;
use Illuminate\Http\Request;

class ShopController extends Controller {
	public function index() {
		// If user not authenticated then just return all shops, no need to filter.
		if (!Auth::check()) {
			$shops = Shop::all();

			return response()->json([
				'status' => 'success',
				'response' => $shops,
			]);
		}

		// Returns shops that were disliked in less than 2 hours ago. We use this to filter out the shops list
		$disliked_shops = Auth::user()->getDislikedShops()->pluck('id')->all();

		// Get liked shops
		$liked_shops = Auth::user()->getLikedShops()->pluck('id')->all();

		// Just merge both arrays as we dont want to show shops disliked in last two hours and also
		// dont want to show liked shops at all.
		$filter = array_merge($disliked_shops, $liked_shops);

		$shops = Shop::whereNotIn('id', $filter)->get();
		$feedbacks = ShopFeedback::whereIn('shop_id', $shops->pluck('id')->all())->get();

		// Inject the user's feedback in
		foreach ($shops as $shop) {
			if ($feedbacks->where('shop_id', $shop->id)->first()) {
				$shop['feedback_type'] = $feedbacks->where('shop_id', $shop->id)->first()->type;
			} else {
				$shop['feedback_type'] = "NONE";
			}
		}

		return response()->json([
			'status' => 'success',
			'response' => $shops,
		]);
	}
}

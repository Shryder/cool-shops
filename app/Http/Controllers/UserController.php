<?php

namespace App\Http\Controllers;

use App\ShopFeedback;
use Auth;
use DB;
use Illuminate\Http\Request;

class UserController extends Controller {
	public function getPreferredShops(Request $request) {
		$preferred_shops = Auth::user()->getLikedShops();

		return response()->json([
			'status' => 'success',
			'response' => $preferred_shops,
		]);
	}

	public function deleteFeedback(Request $request) {
		$request->validate([
			'shop' => 'required|exists:shops,id|max:255',
		]);

		$feedback = ShopFeedback::where('user_id', Auth::id())->where('shop_id', $request->shop)->first();

		if ($feedback) {
			$feedback->type = "NONE";
			$feedback->save();
		} else {
			return response()->json([
				'status' => 'fail',
			]);
		}

		return response()->json([
			'status' => 'success',
		]);
	}

	// Called when a user clicks the like/dislike button (So it could also be called when the user wants to cancel their action)
	public function giveFeedbackOnShop(Request $request) {
		// Some validation
		$request->validate([
			'shop' => 'required|exists:shops,id|max:255',
			'feedback_type' => 'required',
		]);

		$supported_feedbacks = ['NONE', 'LIKED', 'DISLIKED'];
		if (!in_array($request->feedback_type, $supported_feedbacks)) {
			return response()->json([
				'status' => 'fail',
				'response' => 'Invalid feedback_type value provided.',
			]);
		}

		$feedback = ShopFeedback::where('user_id', Auth::id())->where('shop_id', $request->shop)->first();

		if ($feedback) {
			// Row found. Edit feedback type accordingly.
			$feedback->type = $request->feedback_type;
			$feedback->save();
		} else {
			// Row not found. Just create one!
			$feedback = ShopFeedback::create([
				'user_id' => Auth::id(),
				'shop_id' => $request->shop,
				'type' => $request->feedback_type,
			]);
		}

		// Just send back a response with the current state. Clientside stuff will do everything else.
		return response()->json([
			'status' => 'success',
			'response' => $feedback,
		]);
	}
}

<?php

namespace App\Http\Controllers;

use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;

class AuthController extends Controller {

	public function register(Request $request) {

		$validator = Validator::make($request->all(), [
			'email' => 'required|email|unique:users',
			'password' => 'required|min:3|confirmed',
		]);

		if ($validator->fails()) {
			return response()->json([
				'status' => 'error',
				'errors' => $validator->errors(),
			], 422);
		}

		$user = User::create([
			'email' => $request->email,
			'password' => bcrypt($request->password),
		]);

		return response()->json(['status' => 'success'], 200);
	}

	public function login(Request $request) {
		$credentials = $request->only('email', 'password');

		// Return token in Authorization header if authentication succeeded.
		if ($token = $this->guard()->attempt($credentials)) {
			return response()->json(['status' => 'success'], 200)->header('Authorization', $token);
		}

		return response()->json(['error' => 'login_error'], 401);
	}

	public function logout() {
		// Just destroy the token and
		$this->guard()->logout();
		return response()->json([
			'status' => 'success',
			'response' => 'Logged out Successfully.',
		], 200);
	}

	public function user(Request $request) {
		$user = User::find(Auth::user()->id);

		return response()->json([
			'status' => 'success',
			'response' => $user,
		]);
	}

	public function refresh() {
		if ($token = $this->guard()->refresh()) {
			return response()
				->json(['status' => 'successs'], 200)
				->header('Authorization', $token);
		}

		// If anything wrong were to happen
		return response()->json(['error' => 'refresh_token_error'], 401);
	}

	private function guard() {
		return Auth::guard();
	}
}

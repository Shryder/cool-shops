<?php

namespace App;

use App\ShopFeedback;
use Carbon\Carbon;
use DB;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Tymon\JWTAuth\Contracts\JWTSubject;

class User extends Authenticatable implements JWTSubject {
	use Notifiable;

	/**
	 * The attributes that are mass assignable.
	 *
	 * @var array
	 */
	protected $fillable = [
		'name', 'email', 'password',
	];

	/**
	 * The attributes that should be hidden for arrays.
	 *
	 * @var array
	 */
	protected $hidden = [
		'password', 'remember_token',
	];

	/**
	 * The attributes that should be cast to native types.
	 *
	 * @var array
	 */
	protected $casts = [
		'email_verified_at' => 'datetime',
	];

	public function getJWTIdentifier() {
		return $this->getKey();
	}

	public function getJWTCustomClaims() {
		return [];
	}
	/*
		* Query shop_feedback and get list of liked shops by the User
		*
		* @returns Collection
	*/
	public function getLikedShops() {
		$feedbacks = ShopFeedback::where('user_id', $this->id)
			->where('type', 'LIKED')
			->pluck('shop_id')->all();
		return Shop::whereIn('id', $feedbacks)->get();
	}

	/*
		* Query shop_feedback and get list of disliked shops by the User,
		* But only those that are NOT older than 2 hours ago
		*
		* @returns Collection
	*/
	public function getDislikedShops() {
		// Thought of using an Eloquent relationship like $user->preferred_shops but
		// Couldn't find a way to run a where query on a relationship (like $user->feedbacked_shops->whereDate(...))
		// This should do exact same thing anyway
		// $minimum_time_ago = Carbon::parse(time() - (3600 * 2))->toDateTimeString();
		$minimum_time_ago = Carbon::now()->subHours(24)->toDateTimeString();

		return ShopFeedback::where('user_id', $this->id)
			->whereDate('created_at', '<', $minimum_time_ago)
			->where('type', 'DISLIKED')
			->get();
	}
}

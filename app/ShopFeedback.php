<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ShopFeedback extends Model {
	protected $table = 'shop_feedback';

	protected $hidden = ['created_at', 'updated_at'];

	protected $fillable = ['shop_id', 'user_id', 'type'];
}
